<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-develop',
    'version' => 'dev-develop',
    'aliases' => 
    array (
    ),
    'reference' => '525f278d06217c62ce8ef6466dc4069a4c34b097',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-develop',
      'version' => 'dev-develop',
      'aliases' => 
      array (
      ),
      'reference' => '525f278d06217c62ce8ef6466dc4069a4c34b097',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9cf661f4eb38f7c881cac67c75ea9b00bf97b210',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '7.3.0',
      'version' => '7.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7008573787b430c1c1f650e3722d9bba59967628',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e7d04f1f6450fef59366c399cfad4b9383aa30d',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.8.2',
      'version' => '1.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc960a912984efb74d0a90222870c72c87f10c91',
    ),
    'illuminate/collections' => 
    array (
      'pretty_version' => 'v8.58.0',
      'version' => '8.58.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '673d71d9db2827b04c096c4fe9739edddea14ac4',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v8.58.0',
      'version' => '8.58.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7354badf7b57eae805a56d1163fb023e0f58a639',
    ),
    'illuminate/macroable' => 
    array (
      'pretty_version' => 'v8.58.0',
      'version' => '8.58.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '300aa13c086f25116b5f3cde3ca54ff5c822fb05',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v8.58.0',
      'version' => '8.58.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e8f059a5d1f298324e847822b997778a3472128',
    ),
    'intervention/image' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0925f10b259679b5d8ca58f3a2add9255ffcda45',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '1.1.5',
      'version' => '1.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '18634df356bfd4119fe3d6156bdb990c414c14ea',
    ),
    'league/glide' => 
    array (
      'pretty_version' => '1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae5e26700573cb678919d28e425a8b87bc71c546',
    ),
    'league/mime-type-detection' => 
    array (
      'pretty_version' => '1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3b9dff8aaf7323590c1d2e443db701eb1f9aa0d3',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.52.0',
      'version' => '2.52.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '369c0e2737c56a0f39c946dd261855255a6fccbe',
    ),
    'nicmart/tree' => 
    array (
      'pretty_version' => '0.3.1',
      'version' => '0.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c55ba47c64a3cb7454c22e6d630729fc2aab23ff',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'spatie/browsershot' => 
    array (
      'pretty_version' => '3.40.2',
      'version' => '3.40.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '3e55eaf5ab8cee65d1661a567e89b3374afb9116',
    ),
    'spatie/crawler' => 
    array (
      'pretty_version' => '4.7.6',
      'version' => '4.7.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fe6e428b8ceaec561b9819bb948242ad0cfd40bb',
    ),
    'spatie/image' => 
    array (
      'pretty_version' => '1.10.5',
      'version' => '1.10.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '63a963d0200fb26f2564bf7201fc7272d9b22933',
    ),
    'spatie/image-optimizer' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c22202fdd57856ed18a79cfab522653291a6e96a',
    ),
    'spatie/laravel-sitemap' => 
    array (
      'pretty_version' => '5.8.0',
      'version' => '5.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '90c4dd061ba251c2bff9edf83d5e1d38d17f1529',
    ),
    'spatie/robots-txt' => 
    array (
      'pretty_version' => '1.0.10',
      'version' => '1.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '8802a2bee670b3c13cfd21ede0322f72b3efb711',
    ),
    'spatie/temporary-directory' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f517729b3793bca58f847c5fd383ec16f03ffec6',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
    ),
    'symfony/dom-crawler' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c7eef3a60ccfdd8eafe07f81652e769ac9c7146c',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '38f26c7d6ed535217ea393e05634cb0b244a1967',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d595a6d15fd3a2c67f6f31d14d15d3b7356d7a6',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c812666f3e91db75385749fe219c5e494c7f95',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3',
      ),
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v5.3.7',
      'version' => '5.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '3ad5af4aed07d0a0201bbcfc42658fe6c5b2fb8f',
    ),
    'tightenco/collect' => 
    array (
      'pretty_version' => 'v7.26.1',
      'version' => '7.26.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e460929279ad806e59fc731e649e9b25fc8774a',
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '1.5.6',
      'version' => '1.5.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '80953678b19901e5165c56752d087fc11526017c',
    ),
  ),
);
