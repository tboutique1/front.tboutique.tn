import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RechercheProduitSocieteComponent } from './recherche-produit-societe.component';

describe('RechercheProduitSocieteComponent', () => {
  let component: RechercheProduitSocieteComponent;
  let fixture: ComponentFixture<RechercheProduitSocieteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RechercheProduitSocieteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RechercheProduitSocieteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
