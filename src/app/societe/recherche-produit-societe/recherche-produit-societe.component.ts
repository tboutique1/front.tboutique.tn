import {Component, Input, OnInit} from '@angular/core';
import {NewProduitService} from "../../new-produit/service/new-produit.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-recherche-produit-societe',
  templateUrl: './recherche-produit-societe.component.html',
  styleUrls: ['./recherche-produit-societe.component.css']
})
export class RechercheProduitSocieteComponent implements OnInit {

  @Input() societeId;
  public error = null;
  public errors = null;
  public listNewProduit = [];
  public loading = false;
  public showrechercheavance = false;
  public searchObject;

  constructor(private newProduitService: NewProduitService,
              private router: Router,
              private route: ActivatedRoute,) {
    this.societeId = this.route.snapshot.paramMap.get('id');

    this.router.events.subscribe(Event => {
      this.searchObject = "";
      this.listNewProduit = [];
    })
  }

  ngOnInit(): void {
    this.searchObject = "";
  }

  search() {
    if (this.searchObject.length > 2) {
      this.loading = true;
      let obj = {'titre': this.searchObject,'societe_id':this.societeId};
      return this.newProduitService.newProduitSearchWithCriteria(obj).subscribe(
        data => this.handleGetNewProduitResponse(data),
        error => this.handleGetNewProduitError(error)
      );
    }

  }
  searchAvance(obj: any):any {
      this.loading = true;
      obj.societe_id=this.societeId;
      return this.newProduitService.newProduitSearchWithCriteria(obj).subscribe(
        data => this.handleGetNewProduitResponse(data),
        error => this.handleGetNewProduitError(error)
      );
  }

  public handleGetNewProduitResponse(data): any {
    this.error = null;
    this.errors = null;
    this.listNewProduit = data;
    this.loading = false;
  }

  public handleGetNewProduitError(error): any {
    this.loading = false;
    this.error = error.error.message;
    this.errors = error.error.errors;
  }

  public afficherechercheavance(): any {
    this.showrechercheavance = !this.showrechercheavance;
  }

}
