import {Component, OnInit} from '@angular/core';
import {ProfileService} from '../service/profile.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NewProduitService} from "../../new-produit/service/new-produit.service";

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.css']
})
export class ProfileViewComponent implements OnInit {

  public loading = false;
  public loadingNewProduits = false;
  public error;
  public loadingMessage;
  public loadingNewProduitsMessage;
  public profileId;
  public newProduits = [];
  public profile = {
    id: null,
    nom: null,
    prenom: null,
    email: null,
    telephone: null,
    email_verified_at: null,
    created_at: null,
    image_profile_path: null,
    image_profile_name: null,
    image_coverture_path: null,
    image_coverture_name: null,
    etat: null,
    site_web: null,
    site_fb: null,
    sex: null,
    description: null,
    date_de_naissance: null
  };

  constructor(private profileService: ProfileService,
              private router: Router,
              private route: ActivatedRoute,
              private produitService: NewProduitService) {
  }

  ngOnInit(): void {
    this.profileId = this.route.snapshot.paramMap.get('id');
    this.loadData();
  }

  public handleError(error): any {
    this.loading = false;
    this.loadingNewProduits = false;
    this.error = error.error.message;
    if (this.error === 'User does not have the right roles.') {
      // this.router.navigateByUrl('/');
    }
  }

  public handleResponse(data): any {
    this.loadDatanewProduit();
    this.loading = false;
    this.profile = data;
  }

  public handleResponseProduit(data): any {
    this.loadingNewProduits = false;
    this.newProduits = data;
  }

  public loadData(): any {
    if (this.profileId) {
      this.loadingMessage = 'Chargement profile ...';
      this.loading = true;
      this.profileService.getProfileById(this.profileId).subscribe(
        data => this.handleResponse(data),
        error => this.handleError(error)
      );
    } else {
      this.loadingMessage = 'Ce profile n\'existe pas';
    }
  }

  public loadDatanewProduit(): any {
    this.loadingNewProduitsMessage = 'Chargement produits ...';
    this.loadingNewProduits = true;
    this.produitService.newProduitSearchWithCriteria({'user_id': this.profileId}).subscribe(
      data => this.handleResponseProduit(data),
      error => this.handleError(error)
    );
  }

}
