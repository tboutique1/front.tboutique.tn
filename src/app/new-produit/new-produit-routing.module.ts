import {NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AfterLoginService} from "../services/after-login.service";
import {NewProduitIndexComponent} from "./new-produit-index/new-produit-index.component";
import {NewProduitViewComponent} from "./new-produit-view/new-produit-view.component";

let routes: Routes = [
  {path: '', component: NewProduitIndexComponent, canActivate: [AfterLoginService]},
  {path: 'view/:id', component: NewProduitViewComponent,
    data: {
      title: 'Title for First Component',
      descrption: 'Description of First Component'
    }
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewProduitRoutingModule {
}
