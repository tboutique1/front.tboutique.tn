import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {NewProduitService} from "../service/new-produit.service";
import {OwlOptions} from "ngx-owl-carousel-o";
import {SeoTagService} from "../../seo-tag.service";

@Component({
  selector: 'app-new-produit-view',
  templateUrl: './new-produit-view.component.html',
  styleUrls: ['./new-produit-view.component.css']
})
export class NewProduitViewComponent implements OnInit {
  loading = true;
  @Input() newProduit;
  error;
  errors;
  username;
  copierUrl;

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 700,
    navText: [
      '<i class="fas fa-chevron-circle-left"></i>',
      '<i class="fas fa-chevron-circle-right"></i>'
    ],
    autoplayTimeout: 5000,
    autoplay: true,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: true
  };

  constructor(private route: ActivatedRoute,
              private newProduitService: NewProduitService,
              private seoTagService: SeoTagService
  ) {
  }

  ngOnInit(): void {

    var newProduitId = this.route.snapshot.paramMap.get('id');
    if (newProduitId) {
      this.loading = true;
      return this.newProduitService.getNewProduitById(newProduitId).subscribe(
        data => this.handleGetNewProduitResponse(data),
        error => this.handleGetSocieteError(error)
      );
    }
  }

  public handleGetNewProduitResponse(data): any {

    this.newProduit = data;
    if (this.newProduit.user.site_fb) {
      var tab = this.newProduit.user.site_fb.split("/");
      this.username = tab[tab.length - 1];
    }
    if (this.newProduit.societe && this.newProduit.societe.site_fb) {
      var tab = this.newProduit.societe.site_fb.split("/");
      this.username = tab[tab.length - 1];
    }
    this.seoTagService.updateDescription(this.newProduit.description);
    this.seoTagService.updateTitle(this.newProduit.titre);
    this.copierUrl = 'http://api.tboutique.tn/produit/' + this.newProduit.id;
    this.loading = false;
  }

  public handleGetSocieteError(error): any {
    this.loading = false;
    this.error = error.error.message;
    this.errors = error.error.errors;
  }
}
