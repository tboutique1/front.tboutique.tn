import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ModeleService} from "../../modele/service/modele.service";
import {ToastrService} from "ngx-toastr";
import {NewProduitService} from "../service/new-produit.service";
import {SousCategorieService} from "../../sous-categorie/service/sous-categorie.service";
import {MarqueService} from "../../marque/service/marque.service";
import {CategorieService} from "../../categorie/service/categorie.service";

export interface NewProduitType {
  id: string,
  titre: string,
  nom: string,
  description: string,
  quantite: string,
  seuil_min: string,
  prix: string,
  modele_id: string,
  marque_id: string,
  sous_category_id: string,
  category_id: string,
  reference: string,
  paiement_facilite_3_mois: string,
  paiement_facilite_6_mois: string,
  paiement_facilite_12_mois: string,
  prix_achat: string,
  prix_sold: string,
  url_externe: string,
  etat_produit: string,
  etat: number
}

@Component({
  selector: 'app-new-produit-create',
  templateUrl: './new-produit-create.component.html',
  styleUrls: ['./new-produit-create.component.css']
})
export class NewProduitCreateComponent implements OnInit {

  @Input() newProduit: NewProduitType;
  @Output() loadDataAjout: EventEmitter<any> = new EventEmitter<any>();
  @Output() loadDataEdit: EventEmitter<any> = new EventEmitter<any>();
  public error;
  public errors;
  public loading = false;
  public modeleListe = [];
  public marqueListe = [];
  public loadingSousCategorie = false;
  public loadingModele = false;
  public loadingMarque = false;
  public loadingCategorie = false;
  public sousCategorieListe = [];
  public categorieListe = [];

  Nouveau = 'Nouveau';
  Utilise = 'Utilisé';
  Reconditionne = 'Reconditionné';
  public selectedFile: File[] = [];
  public File: File[] = [];
  public FileCouverture: File[] = [];

  constructor(private newProduitService: NewProduitService,
              private modeleService: ModeleService,
              private sousCategorieService: SousCategorieService,
              private marqueService: MarqueService,
              private categorieService: CategorieService,

              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.newProduit = new class implements NewProduitType {
      description: string;
      etat: number;
      etat_produit: string;
      nom: string;
      id: string;
      modele_id: string;
      marque_id: string;
      paiement_facilite_12_mois: string;
      paiement_facilite_3_mois: string;
      paiement_facilite_6_mois: string;
      prix: string;
      prix_achat: string;
      prix_sold: string;
      quantite: string;
      reference: string;
      seuil_min: string;
      sous_category_id: string;
      category_id: string;
      titre: string;
      url_externe: string;
    };
    this.loadingModele = true;
    this.loadingSousCategorie = true;
    this.modeleService.modeleSearchWithCriteria({}).subscribe(
      data => this.handleModeleSearchResponse(data),
      error => this.handleError(error));
    this.marqueService.marqueSearchWithCriteria({}).subscribe(
      data => this.handleMarqueSearchResponse(data),
      error => this.handleError(error));
    this.categorieService.categorieSearchWithCriteria({}).subscribe(
      data => this.handleCategorieSearchResponse(data),
      error => this.handleError(error));
  }
  public handleMarqueSearchResponse(data): any {
    this.loadingMarque = false;
    this.marqueListe = data;
    this.findModele();
  }
  public findModele(): any {
    this.marqueListe.forEach(marque => {
      if (this.newProduit.marque_id == marque.id) {
        this.modeleListe = marque.modeles
      }
    });
  }

  public handleCategorieSearchResponse(data): any {
    this.loadingCategorie = false;
    this.categorieListe = data;
    this.findSousCategorie();
  }

  public handleModeleSearchResponse(data): any {
    this.loadingModele = false;
    this.modeleListe = data;
  }

  public handleSousCategorieSearchResponse(data): any {
    this.loadingSousCategorie = false;
    this.sousCategorieListe = data;
  }


  public onSubmit(): any {
    this.loading = true;
    const formData: FormData = new FormData();

    var autoriseAcion = false;
    if(this.selectedFile.length>5){
      this.selectedFile=[];
    }
    this.FileCouverture.map(x=>this.selectedFile.push(x));
    this.File.map(x=>this.selectedFile.push(x));
    var filesLength = 0;
    if (this.selectedFile) {
      filesLength = this.selectedFile.length
    }
    if (filesLength != 0) {
      if (filesLength > 5) {
        alert("Vous pouvez ajouter au maximum 5 images");
        autoriseAcion = false;
        this.loading = false;
      } else {
        autoriseAcion = true;
        for (let i = 0; i < filesLength; i++) {
          formData.append('selectedFile[]', this.selectedFile[i], this.selectedFile[i].name);
        }
      }
    } else {
      if (this.newProduit.id) {
        autoriseAcion = true;
        formData.append('selectedFile', null);
      } else {
        autoriseAcion = false;
        this.loading = false;
        alert("image est un champ obligatoire");
      }
    }
    if (this.newProduit.description) {
      formData.append('description', this.newProduit.description);
    }
    if (this.newProduit.modele_id) {
      formData.append('modele_id', this.newProduit.modele_id);
    }
    if (this.newProduit.marque_id) {
      formData.append('marque_id', this.newProduit.marque_id);
    }
    formData.append('prix', this.newProduit.prix);
    formData.append('quantite', this.newProduit.quantite);
    formData.append('seuil_min', this.newProduit.seuil_min);
    formData.append('sous_category_id', this.newProduit.sous_category_id);
    formData.append('category_id', this.newProduit.category_id);
    formData.append('titre', this.newProduit.titre);
    if (this.newProduit.paiement_facilite_3_mois) {
      formData.append('paiement_facilite_3_mois', this.newProduit.paiement_facilite_3_mois);
    }
    if (this.newProduit.paiement_facilite_6_mois) {
      formData.append('paiement_facilite_6_mois', this.newProduit.paiement_facilite_6_mois);
    }
    if (this.newProduit.paiement_facilite_12_mois) {
      formData.append('paiement_facilite_12_mois', this.newProduit.paiement_facilite_12_mois);
    }

    formData.append('reference', this.newProduit.reference);
    formData.append('prix_achat', this.newProduit.prix_achat);
    if (this.newProduit.prix_sold) {
      formData.append('prix_sold', this.newProduit.prix_sold);
    }
    if (this.newProduit.url_externe) {
      formData.append('url_externe', this.newProduit.url_externe);
    }
    if (this.newProduit.etat_produit) {
      formData.append('etat_produit', this.newProduit.etat_produit);
    }
    formData.append('etat', '1');

    if (this.newProduit.id) {
      return this.newProduitService.updateNewProduit(this.newProduit.id, formData).subscribe(
        data => this.handleUpdateResponse(data),
        error => this.handleError(error)
      );
    } else {
      this.newProduitService.createNewProduit(formData).subscribe(
        data => this.handleSubmitResponse(data),
        error => this.handleError(error)
      );
    }
    this.selectedFile=[];
  }

  public handleSubmitResponse(data): any {
    this.error = null;
    this.errors = null;
    this.toastr.success(data.message, 'Opération effectuée avec succès',
      {
        closeButton: true,
        progressBar: true,
        progressAnimation: 'increasing'
      });
    this.loading = false;
    this.newProduit = data.data;
    return this.loadDataAjout.emit({});
  }

  public handleUpdateResponse(data): any {
    this.error = null;
    this.errors = null;
    this.toastr.success('produit modifie avec succée', 'Opération effectuée avec succès',
      {
        closeButton: true,
        progressBar: true,
        progressAnimation: 'increasing'
      });
    this.loading = false;
    return this.loadDataEdit.emit({});
  }

  public handleError(error): any {
    console.log(error);
    this.loading = false;
    this.error = error.error.message;
    this.errors = error.error.errors;
  }

  onSelect(event) {
    this.File.push(...event.addedFiles);
  }

  onRemove(event) {
    this.File.splice(this.File.indexOf(event), 1);
  }
  onSelectCouverture(event) {
    this.FileCouverture=[];
    this.FileCouverture.push(...event.addedFiles);
  }

  onRemoveCouverture(event) {
    this.FileCouverture.splice(this.FileCouverture.indexOf(event), 1);
  }
  public findSousCategorie(): any {
    this.categorieListe.forEach(categorie => {
      if (this.newProduit.category_id == categorie.id) {
        this.sousCategorieListe = categorie.sousCategories
      }
    });

  }

}
