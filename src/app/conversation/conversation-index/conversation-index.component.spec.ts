import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationIndexComponent } from './conversation-index.component';

describe('ConversationIndexComponent', () => {
  let component: ConversationIndexComponent;
  let fixture: ComponentFixture<ConversationIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConversationIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
