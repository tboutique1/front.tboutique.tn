import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConversationRoutingModule } from './conversation-routing.module';
import { ConversationListComponent } from './conversation-list/conversation-list.component';
import { ConversationViewComponent } from './conversation-view/conversation-view.component';
import { ConversationIndexComponent } from './conversation-index/conversation-index.component';
import { ConversationCreateComponent } from './conversation-create/conversation-create.component';


@NgModule({
  declarations: [ConversationListComponent, ConversationViewComponent, ConversationIndexComponent, ConversationCreateComponent],
  imports: [
    CommonModule,
    ConversationRoutingModule
  ]
})
export class ConversationModule { }
