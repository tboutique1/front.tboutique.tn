export class GlobalConstants {
  public static COULEUR_VOITURE = [
    {'nom':'Beige','id':'Beige'},
    {'nom':'Blanc','id':'Blanc'},
    {'nom':'Bleu','id':'Bleu'},
    {'nom':'Camel','id':'Camel'},
    {'nom':'Corail','id':'Corail'},
    {'nom':'Doré','id':'Doré'},
    {'nom':'Gris','id':'Gris'},
    {'nom':'Jaune','id':'Jaune'},
    {'nom':'Marron','id':'Marron'},
    {'nom':'Multicolore','id':'Multicolore'},
    {'nom':'Noir','id':'Noir'},
    {'nom':'Orange','id':'Orange'},
    {'nom':'Rose','id':'Rose'},
    {'nom':'Rouge','id':'Rouge'},
    {'nom':'Vert','id':'Vert'},
    {'nom':'Violet','id':'Violet'},
    {'nom':'Autre','id':'Autre'}
  ];
  public static TYPE_CARROSSERIE = [
    {'nom':'Compacte','id':'Compacte'},
    {'nom':'Berline','id':'Berline'},
    {'nom':'Cabriolet','id':'Cabriolet'},
    {'nom':'4 x 4','id':'4 x 4'},
    {'nom':'Monospace','id':'Monospace'},
    {'nom':'Utilitaire','id':'Utilitaire'},
    {'nom':'Pick up','id':'Pick up'},
    {'nom':'Autres','id':'Autres'}

  ];
  public static BOITE = [
    {'nom':'Automatique','id':'Automatique'},
    {'nom':'Manuelle','id':'Manuelle'}
  ];
  public static ETAT_VOITURE = [
    {'nom':'Nouveau','id':'Nouveau'},
    {'nom':'Avec kilométrage','id':'Avec kilométrage'},
    {'nom':'Non dédouanné','id':'Non dédouanné'},
    {'nom':'RS','id':'RS'},
    {'nom':'Pièces manquantes','id':'Manuelle'}
  ];
  public static CARBURANT= [
    {'nom':'Essence','id':'Essence'},
    {'nom':'Diesel','id':'Diesel'},
    {'nom':'Hybride','id':'Hybride'},
    {'nom':'Electrique','id':'Electrique'}
  ];

  public static CYLINDREE = [
    {'nom':'<1.0L','id':'<1.0L'},
    {'nom':' 1.1L ','id':''},
    {'nom':'1.2L','id':'1.2L'},
    {'nom':'1.3L','id':'1.3L'},
    {'nom':'1.4L','id':'1.4L'},
    {'nom':'1.5L','id':'1.5L'},
    {'nom':'1.6L','id':'1.6L'},
    {'nom':'1.7L','id':'1.7L'},
    {'nom':'1.8L','id':'1.8L'},
    {'nom':'1.9L','id':'1.9L'},
    {'nom':'2.0L','id':'2.0L'},
    {'nom':'2.1L','id':'2.1L'},
    {'nom':'2.2L','id':'2.2L'},
    {'nom':'2.3L','id':'2.3L'},
    {'nom':'2.4L','id':'2.4L'},
    {'nom':'2.5L','id':'2.5L'},
    {'nom':'2.6L','id':'2.6L'},
    {'nom':'2.7L','id':'2.7L'},
    {'nom':'2.8L','id':'2.8L'},
    {'nom':'2.9L','id':'2.9L'},
    {'nom':'3.0L','id':'3.0L'},
    {'nom':'3.1L','id':'3.1L'},
    {'nom':'3.2L','id':'3.2L'},
    {'nom':'3.3L','id':'3.3L'},
    {'nom':'3.4L','id':'3.4L'},
    {'nom':'3.5L','id':'3.5L'},
    {'nom':'>4.0L','id':'>4.0L'},
  ];

}
