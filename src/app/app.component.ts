import {Component, OnInit} from '@angular/core';
import {AuthService} from './services/auth.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {TokenService} from './services/token.service';
//import {SocieteService} from "./societe/service/societe.service";
import {CategorieService} from "./categorie/service/categorie.service";
import {CanonicalService} from "./services/canonical.service";
import {Meta, Title} from "@angular/platform-browser";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public loggedIn: boolean;
  public rolesString: string;
  public roles = null;
  public profileImg = null;
  public user: string;
  public adminRole;
  public utilisateurRole;
  public adminSocieteRole;
  public societeStorage;
  public date_fin_abonnement_societe;
  public resNb;
  public afficheif = false;
  public routeHome = false;
  public roueChanceHome = false;
  public error;
  public errors;
  public des=localStorage.getItem('description');
  public categories = [];
  public loading = false;

  constructor(private auth: AuthService,
              private router: Router,
              private token: TokenService,
              private categorieService: CategorieService,
              private canonicalService: CanonicalService,
              private activatedRoute: ActivatedRoute,
              private titleService: Title,
              private metaService: Meta
  ) {
    this.adminRole = false;
    this.utilisateurRole = false;
  }

  dayDiff(d1, d2) {
    d1 = d1.getTime() / 86400000;
    d2 = d2.getTime() / 86400000;
    return new Number(d2 - d1 + 1).toFixed(0);
  }
  getChild(activatedRoute: ActivatedRoute) {
    if (activatedRoute.firstChild) {
      return this.getChild(activatedRoute.firstChild);
    } else {
      return activatedRoute;
    }

  }

  ngOnInit(): void {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
    )
      .subscribe(() => {

        var rt = this.getChild(this.activatedRoute)

        rt.data.subscribe(data => {
          this.titleService.setTitle(data.title)

          if (data.descrption) {
            this.metaService.updateTag({ name: 'description', content: data.descrption })
          } else {
            this.metaService.removeTag("name='description'")
          }

          if (data.robots) {
            this.metaService.updateTag({ name: 'robots', content: data.robots })
          } else {
            this.metaService.updateTag({ name: 'robots', content: "follow,index" })
          }

          if (data.ogUrl) {
            this.metaService.updateTag({ property: 'og:url', content: data.ogUrl })
          } else {
            this.metaService.updateTag({ property: 'og:url', content: this.router.url })
          }

          if (data.ogTitle) {
            this.metaService.updateTag({ property: 'og:title', content: data.ogTitle })
          } else {
            this.metaService.removeTag("property='og:title'")
          }

          if (data.ogDescription) {
            this.metaService.updateTag({ property: 'og:description', content: data.ogDescription })
          } else {
            this.metaService.removeTag("property='og:description'")
          }

          if (data.ogImage) {
            this.metaService.updateTag({ property: 'og:image', content: data.ogImage })
          } else {
            this.metaService.removeTag("property='og:image'")
          }


        })

      })

    this.canonicalService.setCanonicalURL();

    this.loadCategories();
    if (!this.token.loggedIn()) {

      // this.router.navigateByUrl('/');
    } else {
      this.societeStorage = localStorage.getItem('societe');

      this.date_fin_abonnement_societe = localStorage.getItem('date_fin_abonnement_societe');
      setTimeout(() => {
        this.afficheif = this.token.loggedIn();
      }, 2000);

      if (this.date_fin_abonnement_societe) {
        this.date_fin_abonnement_societe = new Date(this.date_fin_abonnement_societe);
        var today = new Date();
        this.resNb = this.dayDiff(today, this.date_fin_abonnement_societe);
      }
      this.rolesString = localStorage.getItem('roles');
      if (this.rolesString) {
        this.roles = this.rolesString.split(",");
      }

      this.user = localStorage.getItem('user');
    }
    if (Array.isArray(this.roles)) {
      if (this.roles.indexOf('admin') !== -1) {
        this.adminRole = true && this.token.loggedIn;
      }
      if (this.roles.indexOf('utilisateur') !== -1) {
        this.utilisateurRole = true && this.token.loggedIn;
      }
      if (this.roles.indexOf('admin_societe') !== -1) {
        this.adminSocieteRole = true && this.token.loggedIn;
      }
    } else {
      if (this.roles === 'admin') {
        this.adminRole = true && this.token.loggedIn;
      } else if (this.roles === 'utilisateur') {
        this.utilisateurRole = true && this.token.loggedIn;
      } else if (this.roles === 'admin_societe') {
        this.adminSocieteRole = true && this.token.loggedIn;
      }

    }
    this.auth.authStatus.subscribe(value => this.loggedIn = value);
    // js
    var isNavbarTop = JSON.parse(localStorage.getItem('isNavbarTop'));
    if (isNavbarTop) {
      var navbarVertical = document.querySelector('.navbar-vertical');
      var navbarTop = document.querySelector('.content .navbar-top');
      if (navbarVertical){
        navbarVertical.parentNode.removeChild(navbarVertical);
      }
      if (navbarTop){
        navbarTop.parentNode.removeChild(navbarTop);
      }
    } else {
      var navbarTop = document.querySelector('[data-layout] .navbar-top');
      if (navbarTop){
        navbarTop.parentNode.removeChild(navbarTop);
      }

    }
    var isFluid = JSON.parse(localStorage.getItem('isFluid'));
    if (isFluid) {
      var container = document.querySelector('[data-layout]');
      container.classList.remove('container');
      container.classList.add('container-fluid');
    }
    if (localStorage.getItem('profileImg') && this.profileImg == null) {
      this.profileImg = localStorage.getItem('profileImg');
    } else {
      setTimeout(() => {
        this.profileImg = localStorage.getItem('profileImg');
      }, 1000);
    }
  }

  public logout(event: MouseEvent): any {
    event.preventDefault();
    this.auth.changeAuthStatus(false);
    this.token.remove();
    localStorage.removeItem('roles');
    localStorage.removeItem('user');
    localStorage.removeItem('profileImg');
    localStorage.removeItem('date_fin_abonnement_societe');
    localStorage.removeItem('societe');
    localStorage.removeItem('isNavbarTop');
    this.router.navigateByUrl('/authentification/login');

  }

  loadCategories() {
    this.categorieService.categorieSearchWithCriteria({'societe_id': 1}).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );

  }

  public handleResponse(data): any {
    this.error = null;
    this.errors = null;
    this.categories = data;
    this.loading = false;
  }

  public handleError(error): any {
    this.loading = false;
    this.error = error.error.message;
    this.errors = error.error.errors;
  }

  onActivate(event) {
    if (this.profileImg == null) {
      this.profileImg = localStorage.getItem('profileImg');
    }

    if (this.router.url.includes('/roue-chance/')) {
      this.roueChanceHome = false;
    } else {
      this.roueChanceHome = true;
    }

    if (this.router.url == '/societe-recherche-avance' ||
      this.router.url.includes('/roue-chance/') ||
      this.router.url.includes('/societe/view/')
    ) {
      this.routeHome = false;
    } else {
      this.routeHome = true;
    }
  }

}
