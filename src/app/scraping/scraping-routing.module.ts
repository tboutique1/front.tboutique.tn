import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ScrapingComponent } from './scraping.component';
import {MarqueIndexComponent} from "../marque/marque-index/marque-index.component";
import {AfterLoginService} from "../services/after-login.service";

const routes: Routes = [{ path: '', component: ScrapingComponent, canActivate: [AfterLoginService] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScrapingRoutingModule { }
