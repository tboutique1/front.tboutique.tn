import {Component, OnInit} from '@angular/core';
import {MarqueService} from "../marque/service/marque.service";
import {ScrapingService} from "./service/scraping.service";
import {CategorieService} from "../categorie/service/categorie.service";

@Component({
  selector: 'app-scraping',
  templateUrl: './scraping.component.html',
  styleUrls: ['./scraping.component.css']
})
export class ScrapingComponent implements OnInit {
  public url;
  public site;
  public error;
  public errors;
  public loading = false;
  public category_id;
  public sous_category_id;
  public sousCategorieListe = [];
  public categorieListe = [];

  constructor(private scrapingService: ScrapingService,
              private categorieService: CategorieService,
  ) {
  }

  ngOnInit(): void {
    return this.categorieService.categorieSearchWithCriteria({}).subscribe(
      data => this.handleCategorieSearchResponse(data),
      error => this.handleError(error));

  }

  public onSubmit(): any {
    this.loading = true;
    if (this.url && this.sous_category_id && this.site) {
      let dataSite = {url: this.url,
        sous_category_id: this.sous_category_id,
        category_id: this.category_id,
        site: this.site};
      return this.scrapingService.createScraping(dataSite).subscribe(
        data => this.handleResponse(data),
        error => this.handleError(error)
      );
    }
  }

  public handleResponse(data): any {
    this.loading = false;
    this.error = null;
    this.errors = null;
  }

  public handleError(error): any {
    console.log(error);
    this.loading = false;
    this.error = error.error.message;
    this.errors = error.error.errors;
  }
  public findSousCategorie(): any {
    this.categorieListe.forEach(categorie => {
      if (this.category_id == categorie.id) {
        this.sousCategorieListe = categorie.sousCategories
      }
    });
  }
  public handleCategorieSearchResponse(data): any {
    this.categorieListe = data;
    this.findSousCategorie();
  }

}
