import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScrapingRoutingModule } from './scraping-routing.module';
import { ScrapingComponent } from './scraping.component';
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [ScrapingComponent],
  imports: [
    CommonModule,
    ScrapingRoutingModule,
    FormsModule
  ]
})
export class ScrapingModule { }
