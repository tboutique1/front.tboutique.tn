export const environment = {
  production: true,
  baseUrl : "https://api.tboutique.tn/api",
  frontUrl : "https://www.tboutique.tn"
};
